use std::collections::BTreeMap;

use futures::prelude::*;
use irc::client::prelude::{Client, Command};
use youtube_dl::YoutubeDl;

use tracing::{debug, error};

use anyhow::{anyhow, Context, Result};

struct Bot {
    mpd_client: mpd::Client,
    irc_client: irc::client::Client,
    config: irc::client::prelude::Config,
}

use envconfig::Envconfig;

#[derive(Envconfig)]
struct Config {
    #[envconfig(from = "IRC_RADIO_MPD_HOST")]
    pub mpd_host: String,
    #[envconfig(from = "IRC_RADIO_MPD_PORT", default = "6600")]
    pub mpd_port: String,
    #[envconfig(from = "IRC_RADIO_CONFIG", default = "/etc/irc-radio/config.json")]
    pub irc_config: String,
}

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("playlists are not supported")]
    PlaylistsNotSupported,
}

impl Bot {
    async fn new() -> Result<Self> {
        let config = Config::init_from_env().unwrap();

        let mut mpd_client =
            mpd::Client::connect(format!("{}:{}", config.mpd_host, config.mpd_port)).unwrap();
        mpd_client.consume(true).unwrap();

        let config = irc::client::prelude::Config::load(config.irc_config).unwrap();
        let config2 = config.clone();
        let irc_client = Client::from_config(config.clone()).await?;

        irc_client.send_cap_req(&vec![irc::client::prelude::Capability::Sasl])?;
        irc_client.send(Command::NICK(config.nickname.clone().unwrap()))?;
        irc_client.send(Command::USER(
            config.nickname.unwrap(),
            "0".to_owned(),
            config.realname.unwrap(),
        ))?;
        irc_client.send_sasl_plain()?;

        Ok(Self {
            irc_client,
            mpd_client,
            config: config2,
        })
    }

    async fn add_song(&mut self, uri: &str) -> Result<mpd::Song> {
        let metadata = YoutubeDl::new(uri).extract_audio(true).run()?;

        match metadata {
            youtube_dl::YoutubeDlOutput::Playlist(_) => Err(anyhow!("playlists are not supported")),
            youtube_dl::YoutubeDlOutput::SingleVideo(metadata) => {
                let mut tags = BTreeMap::new();

                tags.insert("title".to_string(), metadata.title.clone());

                if let Some(track) = metadata.track_number {
                    tags.insert("track".to_string(), track);
                };

                if let Some(album) = metadata.album {
                    tags.insert("album".to_string(), album);
                };

                if let Some(artist) = metadata.artist {
                    tags.insert("artist".to_string(), artist.clone());
                    tags.insert("performer".to_string(), artist);
                } else if let Some(artist) = metadata.uploader {
                    tags.insert("artist".to_string(), artist.clone());
                    tags.insert("performer".to_string(), artist);
                };

                if let Some(album_artist) = metadata.album_artist {
                    tags.insert("albumartist".to_string(), album_artist);
                };

                let song = mpd::song::Song {
                    file: metadata.url.clone().context("no url found")?,
                    title: Some(metadata.title),
                    tags: tags.clone(),
                    ..mpd::song::Song::default()
                };

                debug!("{:#?} {:#?}", metadata.url.expect("no url found"), tags);

                let song_id = self.mpd_client.push(song.clone())?;

                tags.iter().for_each(|(k, v)| {
                    self.mpd_client
                        .tag(song_id, k, v)
                        .context("could not add tag")
                        .unwrap()
                });

                self.mpd_client.play()?;

                return Ok(song);
            }
        }
    }

    async fn get_playlist(&mut self) -> Result<Vec<mpd::Song>> {
        Ok(self.mpd_client.queue()?)
    }

    async fn clear(&mut self) -> Result<()> {
        Ok(self.mpd_client.clear()?)
    }

    async fn rm_song(&mut self, song: u32) -> Result<()> {
        Ok(self.mpd_client.delete(song - 1)?)
    }
}

#[tokio::main]
async fn main() -> Result<(), anyhow::Error> {
    tracing_subscriber::fmt()
        .compact()
        .with_span_events(tracing_subscriber::fmt::format::FmtSpan::FULL)
        .with_max_level(tracing::Level::DEBUG)
        .with_thread_ids(true)
        .init();

    let mut bot = Bot::new().await?;

    let mut stream = bot.irc_client.stream()?;

    while let Some(message) = stream.next().await.transpose()? {
        debug!(target: "messages", "{}", message);

        match message.command {
            Command::PRIVMSG(channel, text) => {
                if text.starts_with('$') {
                    let msg: Vec<&str> = text.split_whitespace().collect();

                    let nick = match message.prefix.as_ref().unwrap() {
                        irc::client::prelude::Prefix::Nickname(nick, _, _) => nick,
                        _ => break,
                    };

                    match msg[0] {
                        "$listen" => bot.irc_client.send_privmsg(
                            &channel,
                            format!("https://radio.gnulag.net/stream.opus"),
                        )?,
                        "$play" => match bot.mpd_client.play() {
                            Ok(_) => (),
                            Err(e) => bot.irc_client.send_notice(
                                &nick,
                                format!("error: failed to resume playing: {}", e),
                            )?,
                        },
                        "$skip" => match bot.mpd_client.next() {
                            Ok(_) => (),
                            Err(e) => bot.irc_client.send_notice(
                                &nick,
                                format!("error: failed to resume playing: {}", e),
                            )?,
                        },
                        "$pause" => match bot.mpd_client.pause(true) {
                            Ok(_) => (),
                            Err(e) => bot
                                .irc_client
                                .send_privmsg(&channel, format!("error: failed to pause: {}", e))?,
                        },
                        "$add" => match bot.add_song(msg[1]).await {
                            Ok(song) => bot
                                .irc_client
                                .send_notice(&nick, format!("added {}", song.title.unwrap()))?,
                            Err(e) => bot.irc_client.send_privmsg(
                                &channel,
                                format!("error: failed to add song: {}", e),
                            )?,
                        },
                        "$rm" => match bot.rm_song(msg[1].parse().unwrap()).await {
                            Ok(_) => bot.irc_client.send_notice(&nick, format!("removed song"))?,
                            Err(e) => bot.irc_client.send_privmsg(
                                &channel,
                                format!("error: failed to remove song: {}", e),
                            )?,
                        },
                        "$list" => match bot.get_playlist().await {
                            Ok(r) => {
                                if let irc::client::prelude::Prefix::Nickname(nick, _, _) =
                                    message.prefix.unwrap()
                                {
                                    r.iter().take(10).for_each(|song| {
                                        bot.irc_client
                                            .send_notice(
                                                &nick,
                                                format!(
                                                    "{:?}. {}",
                                                    match song.place {
                                                        Some(place) => place.pos + 1,
                                                        None => 0,
                                                    },
                                                    match song.title.as_ref() {
                                                        Some(title) => title,
                                                        None => "<no title>",
                                                    }
                                                ),
                                            )
                                            .expect("failed sending message");
                                    })
                                }
                            }
                            Err(e) => bot.irc_client.send_privmsg(
                                &channel,
                                format!("error: failed to list playlist: {}", e),
                            )?,
                        },
                        "$clear" => match bot.clear().await {
                            Ok(_) => bot
                                .irc_client
                                .send_notice(&nick, format!("cleared playlist"))?,
                            Err(e) => bot.irc_client.send_privmsg(
                                &channel,
                                format!("error: failed to clear playlist: {}", e),
                            )?,
                        },
                        "$help" => {
                            if let irc::client::prelude::Prefix::Nickname(nick, _, _) =
                                message.prefix.unwrap()
                            {
                                bot.irc_client.send_notice(
                                    &nick,
                                    "$add <url> - add a song to the playlist, has to be compatible with youtube-dl",
                                )?;
                                bot.irc_client.send_notice(
                                    &nick,
                                    "$rm <num> - remove the song from the queue",
                                )?;
                                bot.irc_client
                                    .send_notice(&nick, "$list - list the songs in the queue")?;
                                bot.irc_client
                                    .send_notice(&nick, "$skip - skip to the next song")?;
                                bot.irc_client
                                    .send_notice(&nick, "$listen - show the stream URL")?;
                                bot.irc_client
                                    .send_notice(&nick, "$clear - clear the queue")?;
                                bot.irc_client.send_notice(
                                    &nick,
                                    "$play / $pause - you can guess this one",
                                )?;
                            }
                        }
                        cmd => {
                            if let irc::client::prelude::Prefix::Nickname(nick, _, _) =
                                message.prefix.unwrap()
                            {
                                bot.irc_client.send_notice(
                                    nick,
                                    format!("unknown command {}. $help for help", cmd),
                                )?;
                            }
                        }
                    }
                }
            }
            Command::AUTHENTICATE(text) => {
                use sasl::client::mechanisms::Plain;
                use sasl::client::Mechanism;
                use sasl::common::Credentials;

                if text == "+" {
                    let creds = Credentials::default()
                        .with_username(bot.config.clone().username.unwrap())
                        .with_password(bot.config.clone().nick_password.unwrap());

                    let mut mechanism = Plain::from_credentials(creds).unwrap();

                    let initial_data = mechanism.initial();

                    bot.irc_client.send_sasl(base64::encode(initial_data))?;
                    bot.irc_client.send(Command::CAP(
                        None,
                        irc_proto::command::CapSubCommand::END,
                        None,
                        None,
                    ))?;
                }
            }
            _ => (),
        }
    }

    Ok(())
}
